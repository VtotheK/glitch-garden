﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenderSpawner : MonoBehaviour {
    TheDefender defender;
    [SerializeField]GridOccupied gridOccupied;
    [SerializeField]NotEnoughResourcesPopUp resourceErrorScreen;
    [SerializeField]StarDisplay starDisplay;
    bool[,] GridIsOccupied = new bool[10, 10];
    int x = 0, y = 0;
    private void OnMouseDown()
    {
        SpawnDefender(GetSquareClicked());
    }

    private Vector2 GetSquareClicked()
    {
        Vector2 clickPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        Vector2 worldPos = Camera.main.ScreenToWorldPoint(clickPos);
        Vector2 gridPos = SnapToGrid(worldPos);
        return gridPos;
    }

    private Vector2 SnapToGrid(Vector2 rawWorldPos)
    {
        float newX = Mathf.RoundToInt(rawWorldPos.x);
        float newY = Mathf.RoundToInt(rawWorldPos.y);
        return new Vector2(newX, newY);
    }

    private void SpawnDefender(Vector2 roundedPos)
    {
        if (!defender) { return; }
        if (CheckResources() && !IsGridFree(roundedPos.x, roundedPos.y))
        {
            TheDefender _defender = Instantiate(defender, roundedPos, transform.rotation) as TheDefender;
            _defender.GetComponent<Health>().SetSpawnPlace(x, y);
            starDisplay.SetStarAmount(-defender.StarCost());
        }
    }

    private bool CheckResources()
    {
        if (starDisplay.GetStarAmount() < defender.StarCost())
        {
           resourceErrorScreen.WindowNotEnoughResources();
            return false;
        }
        else { return true; }
    }

    public void SetSelectedDefender (TheDefender defenderToSelect)
    {
        defender = defenderToSelect; 
    }

    private bool IsGridFree(float x, float y)
    {
        this.x = Convert.ToInt32(x);
        this.y = Convert.ToInt32(y);
        if (GridIsOccupied[this.x, this.y] == false)
        {
            GridIsOccupied[this.x, this.y] = true;
            return false;
        }
        else
        {
            gridOccupied.WindowGridOccupied();
            return true;
        }
    }

    public void SetGridSpotFree(int x, int y)
    {
        GridIsOccupied[x, y] = false;
    }
}
