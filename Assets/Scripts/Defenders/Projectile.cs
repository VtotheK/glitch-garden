﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {
    [SerializeField] float projectileSpeed = 1f;
    [SerializeField] int damage = 100;
	void Update () {
        moveProjectile();
	}

    private void moveProjectile()
    {
        transform.Translate(Vector2.right * Time.deltaTime * projectileSpeed);
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        var health = collision.GetComponent<Health>();
        var attacker = collision.GetComponent<Attacker>();
        if (health && attacker)
        {
            health.DealDamage(damage);
            Destroy(gameObject);
        }
    }
}
