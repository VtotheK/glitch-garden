﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DefenderCostText : MonoBehaviour {
    TextMeshProUGUI text;
	// Use this for initialization
	void Start () {
        text = GetComponent<TextMeshProUGUI>();
        text.text =  GetComponentInParent<TheDefender>().StarCost().ToString();
	}
}
