﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trophy : MonoBehaviour {
    [SerializeField] int delayInSeconds = 5;
    [SerializeField] GameObject starPrefab;


    private void SpawnStar() //ANIMATION EVENT, DO NOT RENAME
    {
        var _starPrefab = Instantiate(starPrefab, transform.position, transform.rotation);
        Destroy(_starPrefab,1);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
