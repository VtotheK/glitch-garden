﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour {
    private int currentEnemies;
    [SerializeField] GameObject winLabel;
    [SerializeField] GameObject loseLabel;
    bool LevelTimerIsFinished = false;
    bool gameQuitting = false;
    bool attackersTriggeredDestroyer = false;
    private void Start()
    {
        winLabel.SetActive(false);
        loseLabel.SetActive(false);
    }

    private void Update()
    {
        if (LevelTimerIsFinished) { LevelTimerFinished(); }
        if (attackersTriggeredDestroyer) { LoadLostGameUI(); }
    }

    private void LoadLostGameUI()
    {
        loseLabel.SetActive(true);
        Time.timeScale = 0;
        attackersTriggeredDestroyer = false;
    }

    public void SetAttackersTriggeredDestroyer(bool value)
    {
        attackersTriggeredDestroyer = value;
    }

    public void DecreseEnemyCount()
    {
        currentEnemies--;
    }

    public void IncreaseEnemyCount()
    {
        currentEnemies++;
    }

    public int GetCurrentEnemies()
    {
        return currentEnemies;
    }

    public void SetLevelTimerFinish(bool value) { LevelTimerIsFinished = value; }

    private void LevelTimerFinished()
    {
        if (currentEnemies <= 0 && !gameQuitting)
        {
            StartCoroutine(NextLevel());
        }
    }

    IEnumerator NextLevel()
    {
        gameQuitting = true;
        GetComponent<AudioSource>().Play();
        winLabel.SetActive(true);
        yield return new WaitForSeconds(4);
        FindObjectOfType<SceneLoader>().LoadNextScene();
    }
}
