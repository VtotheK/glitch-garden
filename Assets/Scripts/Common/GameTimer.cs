﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameTimer : MonoBehaviour {
    [Tooltip("Level timer in seconds")]
    [SerializeField] float levelTime = 10f;
    bool timerFinished;
    bool gameFinishTriggered = false;
    LevelController levelController;
    // Update is called once per frame

    private void Start()
    {
        levelController = FindObjectOfType<LevelController>();
    }

    void Update () {
        if (gameFinishTriggered) { return; }
        GetComponent<Slider>().value = Time.timeSinceLevelLoad / levelTime;
        timerFinished = (Time.timeSinceLevelLoad >= levelTime);
        if (timerFinished)
        {
            StopAllSpawners();
            levelController.SetLevelTimerFinish(true);
        }
    }



    private static void StopAllSpawners()
    {
        AttackerSpawner[] allSpawners = FindObjectsOfType<AttackerSpawner>();
        foreach (AttackerSpawner spawner in allSpawners)
        {
            spawner.StopSpawning();
        }
    }
}
