﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class StarDisplay : MonoBehaviour {
    [SerializeField] int currentStarAmount = 100;
    // Use this for initialization
    TextMeshProUGUI starText;
	void Start () {
        starText = GetComponentInChildren<TextMeshProUGUI>();
        UpdateStarDisplay();
    }   
	

    private void UpdateStarDisplay()
    {
        starText.text = currentStarAmount.ToString();
    }

    public void SetStarAmount(int amount)
    {
        currentStarAmount += amount;
        UpdateStarDisplay();
    }

    public int GetStarAmount() { return currentStarAmount; }
}
