﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {
    int currentSceneIndex;
    [SerializeField] int timeToWait = 0; 
    private void Start()
    {
        currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        if (currentSceneIndex == 0)
        {
            Time.timeScale = 1;
            LoadGameScene();
        }
    }

    public void ReLoadGameScene()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(2);
    }

    public void LoadGameScene()
    {
        Time.timeScale = 1;
        StartCoroutine(LoadingScreen(timeToWait));
    }

    IEnumerator LoadingScreen(int timeToWait)
    {
        yield return new WaitForSeconds(timeToWait);
        LoadNextScene();
    }

    public void LoadOptionsScreen()
    {
        SceneManager.LoadScene(4);
    }

    public void LoadNextScene()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(currentSceneIndex + 1);
    }

    public void LoadStartScene()
    {
        SceneManager.LoadScene(1);
    }

    public void LoadSplashScreen()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
