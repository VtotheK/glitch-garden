﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Star : MonoBehaviour {
    [SerializeField] int starAmount = 50;

    private void Start()
    {
        FindObjectOfType<StarDisplay>().SetStarAmount(starAmount);
        Destroy(gameObject, 1);
    }

}
