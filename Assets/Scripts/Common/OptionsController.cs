﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsController : MonoBehaviour {
    [SerializeField]Slider volumeSlider;
    [Range(0,1)][SerializeField]float defaultVolume = 0.8f;

    MusicPlayer musicPlayer;
	// Use this for initialization
	void Start () {
        volumeSlider.value = PlayerPrefsController.GetMasterVolume();
        musicPlayer = FindObjectOfType<MusicPlayer>();
    }
	
	// Update is called once per frame
	void Update () {
        if(musicPlayer)
        {
            musicPlayer.SetVolume(volumeSlider.value);
        }
        else
        {
            Debug.LogWarning("No music player found... did you start from the splash screen?");
        }
	}

    public void SaveAndExit()
    {
        PlayerPrefsController.SetMasterVolume(volumeSlider.value);
        FindObjectOfType<SceneLoader>().LoadStartScene();
    }

    public void SetDefaults()
    {
        volumeSlider.value = defaultVolume;
        PlayerPrefsController.SetMasterVolume(defaultVolume);
    }
}
