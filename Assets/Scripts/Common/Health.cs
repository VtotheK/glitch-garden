﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {
    [SerializeField] float health = 100f;
    [SerializeField] GameObject deathVFX;
    int x = 0, y = 0;
    public void DealDamage(float damage)
    {
        health -= damage;
        if (health <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        FindObjectOfType<DefenderSpawner>().SetGridSpotFree(x, y);
        PlayDeathVFX();
        Destroy(gameObject);
    }

    private void PlayDeathVFX()
    {
        if (!deathVFX) { return; }
        GameObject death = Instantiate(deathVFX, transform.position, transform.rotation);
        Destroy(death, 1f);
    }


    public void SetSpawnPlace(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
}
