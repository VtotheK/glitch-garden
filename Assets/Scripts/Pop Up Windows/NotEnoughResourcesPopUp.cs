﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotEnoughResourcesPopUp : MonoBehaviour {
    [SerializeField] GameObject resourceScreen;
	public void WindowNotEnoughResources()
    {
        resourceScreen.SetActive(true);
        StartCoroutine(ShowScreen(resourceScreen));
    }

    private IEnumerator ShowScreen(GameObject screen)
    {
        yield return new WaitForSeconds(1.5f);
        screen.SetActive(false);
    }
}
