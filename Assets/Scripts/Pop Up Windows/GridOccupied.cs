﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridOccupied : MonoBehaviour {
    [SerializeField] GameObject gridSelectedScreen;

    public void WindowGridOccupied()
    {
        gridSelectedScreen.SetActive(true);
        StartCoroutine(ActivateScreen(gridSelectedScreen));
    }

    IEnumerator ActivateScreen(GameObject screen)
    {
        yield return new WaitForSeconds(1.5f);
        screen.SetActive(false);
    }
}
