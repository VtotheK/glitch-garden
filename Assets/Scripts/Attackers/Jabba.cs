﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jabba : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.GetComponent<TheDefender>()) { return; }
        if(collision.GetComponent<Health>())
        {
            GetComponent<Attacker>().Attack(collision.gameObject);
        }
        else { return; }
    }
}
