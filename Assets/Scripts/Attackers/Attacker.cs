﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attacker : MonoBehaviour {
    [SerializeField] int damage = 100;

    float currentSpeed;
    GameObject currentTarget;
    Animator animator;

    private void Awake()
    {
        FindObjectOfType<LevelController>().IncreaseEnemyCount();
    }
    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    private void ChangeAnimationState()
    {
        
        if(!currentTarget)
        {
            animator.SetBool("isAttacking", false);
        }
    }

    private void OnDestroy()
    {
        LevelController levelController = FindObjectOfType<LevelController>();
        if(levelController != null)
        {
            levelController.DecreseEnemyCount();
        }
    }

    void Update () {
        transform.Translate(Vector2.left * Time.deltaTime * currentSpeed); // MOVES ENEMY TO THE LEFT
        ChangeAnimationState();
    }

    public void SetMovementSpeed(float speed) //DONT CHANGE METHOD NAME
    {
        currentSpeed = speed;
    }

    public void Attack(GameObject target)
    {
        animator.SetBool("isAttacking", true);
        currentTarget = target;
    } 

    public void Jump(GameObject target)
    {
        animator.SetBool("isJumping", true);
        currentTarget = target;
    }

    public void StrikeCurrentTarget(float damage)
    {
        if (!currentTarget) { return; }
        Health health = currentTarget.GetComponent<Health>();
        if(health)
        {
            health.DealDamage(damage);
        }
    }
}
