﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackerSpawner : MonoBehaviour {
    [Header("Spawn Time Parameters")]
    [SerializeField] float spawnMinDelay = 5f;
    [SerializeField] float spawnMaxDelay = 15f;
    [Header("EnemyPrefabs")]
    [SerializeField] Attacker[] attackerPrefab;
    [SerializeField]bool spawn = false;

    bool first = true;
    // Use this for initialization

    private IEnumerator Start()
    {
        while (spawn)
        {
            if (first)
            {
                yield return new WaitForSeconds(5);
                first = false;
            }
            else
            {
                yield return new WaitForSeconds(UnityEngine.Random.Range(spawnMinDelay, spawnMaxDelay));
                SpawnEnemy();
            }
        }
    }

    private void SpawnEnemy()
    {
        Attacker newAttacker = Instantiate
            (attackerPrefab[UnityEngine.Random.Range(0,attackerPrefab.Length)],
            transform.position, Quaternion.identity) 
            as Attacker;
        newAttacker.transform.parent = transform;
    }

    public void StopSpawning(){ spawn = false; }

    // Update is called once per frame
    void Update () {
		
	}
}
