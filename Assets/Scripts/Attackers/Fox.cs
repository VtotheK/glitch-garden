﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fox : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D otherCollider)
    {
        if (!otherCollider.GetComponent<TheDefender>()) { return; }
        if (otherCollider.tag == "Gravestone")
        {
            GetComponent<Attacker>().Jump(otherCollider.gameObject);
        }
        else if(otherCollider.GetComponent<TheDefender>())
        {
            GetComponent<Attacker>().Attack(otherCollider.gameObject);
        }
    }

    private void OnTriggerStay2D(Collider2D otherCollider)
    {
        if (!otherCollider.GetComponent<TheDefender>()) { return; }
        if (otherCollider.tag == "Gravestone")
        {
            GetComponent<Attacker>().Jump(otherCollider.gameObject);
        }
        else if (otherCollider.GetComponent<TheDefender>())
        {
            GetComponent<Attacker>().Attack(otherCollider.gameObject);
        }
    }

    public void StartRunning()
    {
        GetComponent<Animator>().SetBool("isJumping", false);
    }
}
